import torch
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torch.nn as nn
import PIL
from PIL import Image
import matplotlib.pyplot as plt

#---------------Loading and preprocessing the data-------------------------------------
def flatten(inp):
    return inp.reshape(-1)
transform = transforms.Compose([transforms.ToTensor(), flatten])

#Get the training and testing datasets
trainingDataset = datasets.MNIST(root='./', train=True, download=False, transform=transform)
testingDataset = datasets.MNIST(root='./', train=True, download=False, transform=transform)

#
trainingLoader = torch.utils.data.DataLoader(trainingDataset, batch_size=128, shuffle=True)
testingLoader = torch.utils.data.DataLoader(testingDataset, batch_size=128, shuffle=True)

#---------------Create Model----------------------------------
#Nueral network with one hidden layer and uses the sigmoid activation function
print("Creating model with one hidden layer with 64 nodes and uses Tanh activation function")
Net = nn.Sequential(
        nn.Linear(28*28, 64),
        nn.Tanh(),
        nn.Linear(64, 10))

optimizer = torch.optim.SGD(Net.parameters(), lr=0.001)
loss_function = nn.CrossEntropyLoss()

#---------------train model-----------------------------------
print("Training the Model..")
epoch = 10

for epoch in range(epoch):
    print(epoch)
    correct = 0
    current_loss = 0
    total = 0
    
    Net.train()
    
    for x, y in trainingLoader:
        optimizer.zero_grad()
        output = Net(x.view(-1, 28*28))
        loss = loss_function(output, y)
        loss.backward() #updating weights using backwward propagation
        optimizer.step()
        current_loss += loss.item()

with torch.no_grad():
    for x, y in testingLoader:
        output = Net(x.view(-1, 28*28))
    for index, i in enumerate(output):
        if torch.argmax(i) == y[index]:
            correct += 1
        total += 1
    
print(f'accuracy: {round(correct/total, 3)}')
print("Done!")
   
#---------------run program-----------------------------------
image_path = input("Please enter a filepath:\n>")
while image_path != "exit":
    try:
        image = Image.open(image_path)
        image = transform(image).float()
        image = image.view(1, 28*28)
        with torch.no_grad():
            output = Net(image)
    
        print(f"Classifier: {torch.argmax(output).numpy()}")
    except:
        print("Cannot find file")

    image_path = input("Please enter a filepath:\n>")

print("Exiting...")

