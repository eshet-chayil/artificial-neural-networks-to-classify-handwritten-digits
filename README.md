# Artificial Neural Networks to Classify Handwritten Digits
***

This program uses ANNs to classify handwritten digits

## Table of contents
1. [Installation](#installation)

## Installation

First make sure you have the following files in the same directory:
* Makefile : For automated compilation

* Requirements : Has the dependencies to be downloaded

* Classifier_1.py : Has a model that uses the sigmoid activation function 

* Classifier_2.py : Has a model that uses the ReLU activation function 

* Classifier_3.py : Has a model that uses the Tanh activation function

* MNIST : Folder with the training and testing dataset

To compile and run the the program:
```
$ make install --- to build the virtual environment and download dependencies
$ python3 Classifier_{suffix}.py ----- to run the ANNs

```
$ exit ---- to exit the program
